package gla.ac.uk.astro.tagtalk;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class HomeActivity extends Activity implements OnItemSelectedListener {
	
	
	/** This is the screen that lets the user select the lecture they wish to tag
	 *  Needs to read the time from the time selector
	 */
	public String lecture;
	public String time;
	public Date day = new Date();
	public static String MESSAGE = "";
	private String lectures = "";
    private ArrayList<String> courseInfo = new ArrayList<String>();
    private List<String> courses = new ArrayList<String>();
 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		//List<String> courses;
		
		//String[] courses = null;
		//List<String> courses = new ArrayList<String>();
		
		SharedPreferences sp = this.getSharedPreferences("courseList",0);
		boolean exists = sp.contains("courseList");
      
		  
				if(exists){
					String s = sp.getString("courseList", "");
					
					String[] str = s.split("next:"); 
					
					//courses = new String[str.length];
										
					for(int i=1; i < str.length; i ++){
						courses.add(str[i]);
					}
						
				}
				else{
					courses.add("No Avaialble Course, Please Reload");
				}
				
		fillSpinner(courses);
		
	}

	private void fillSpinner(List<String> courses) {
		
		/**Fill the spinner with all available courses */
		Spinner spinner = (Spinner) findViewById(R.id.spinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, courses);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);
		
		spinner.setOnItemSelectedListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// this creates the menu, depending on the items in the home menu
		getMenuInflater().inflate(R.menu.activity_home, menu);
		return true;
	}

	
	/** BEGIN SECTION - HANDLE MENU BUTTON CLICKS **/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
	    case R.id.button_home:
	        home();
	        return true;
	    case R.id.my_tags:
	    	myTags();
	    	return true;
	    default:
	        return super.onOptionsItemSelected(item);
		}
	}
	   
	public void home(){
			Intent intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
		}

		
    public void myTags() {
        	
        	Intent intent = new Intent(this, MyTagsActivity.class);
            startActivity(intent);   
        }
		
	/** END SECTION **/
	
	
	//takes to the tagging page and sends details of what is being tagged
	public void startTag(View view){
		
    	String message = lecture;
    	
    	Intent intent = new Intent(this, TagActivity.class);
    	intent.putExtra(MESSAGE, message);
    	startActivity(intent);
		
	}


    public void onItemSelected(AdapterView<?> parent, View view, 
            int pos, long id) {

        lecture = (String) parent.getItemAtPosition(pos);
		
	}

	public void onNothingSelected(AdapterView<?> arg0) {
		// if nothing is selected then do nothing
	}




    /** test server calls **/
        
    	/* Called when the user clicks the lost button */
        public void getCourses (View view) {
        	
        	
        	//clear the lists and the spinner
        	courses = new ArrayList<String>();
        	courseInfo = new ArrayList<String>();   
        	lectures = "";
        	fillSpinner(courses);
        	
            //start the request to the server
        	String[] url = new String[]{"http://ptolemy.astro.gla.ac.uk/podcasting/track"};
        	new availableCourses().execute(url);
        	
        	//alert the user that is is currently reloading, so please wait   

    		Toast.makeText(getApplicationContext(), "Updating the course list, this may take a few moments.", Toast.LENGTH_LONG).show();
        	

        }
        
        public void updatePrefs(){
        	//Now we want to add the new courses to the Shared Prefs
        	SharedPreferences spref = this.getSharedPreferences("courseInfo",0);
        	
        	
        	for(int i=0; i < courses.size(); i++){
        		spref.edit().putString(courses.get(i), courseInfo.get(i)).commit();
        	}
                	  		        
        	SharedPreferences sp = this.getSharedPreferences("courseList",0);
	        sp.edit().putString("courseList", lectures).commit();
        }
        
        
        private class availableCourses extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
            	 try {
            		    HttpClient client = new DefaultHttpClient();  
            		    HttpGet get = new HttpGet(params[0]);
            		   
            		    HttpResponse responseGet = client.execute(get);  
            		    HttpEntity resEntityGet = responseGet.getEntity();  
            		    if (resEntityGet != null) {  
            		        // do something with the response
            		        String response = EntityUtils.toString(resEntityGet);
            		        return response;
            		    }
            	 }
            	catch(Exception e){
                  
            	}
            	 return "message";
            }      
            
            @Override
            protected void onPostExecute(String message){
            	
            	String[] temp = message.split("<li>");
		        
		        int length = temp.length;       
		        
		        for(int i = 1; i<length; i++){
		        	
		        	String[] t = temp[i].split("href=");
		        	String s = t[1]; //this contains the course name and the url
		        	
		        	String url, name;
		        	
		        	t = s.split(">");
		        	url = t[0];
		        	name = t[1].replace("</a", "");
		        	
		        	//add these to the correct lists
		        	lectures = lectures + "next:"  + name;
		        	courseInfo.add("[" + name + "+" + url+"]");        	
		        	courses.add(name);
		        	
		        	
		        } 
		        
		       //now fill the spinner
		       fillSpinner(courses);
		       updatePrefs();
            	 
            }
        }
}
