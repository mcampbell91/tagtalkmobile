package gla.ac.uk.astro.tagtalk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;



@SuppressLint("NewApi")
public class MyTagsActivity extends Activity {

	private ListView listview;
	private String[] urls = new String[1];
	private String[] name = new String[1];
	private ArrayList<String> tagList;
	private ArrayList<String[]> uploadTags = new ArrayList<String[]>();
	private HashMap<String, ArrayList<String>> available;
	private ArrayList<String> lectures;
	private String baseURL = "http://ptolemy.astro.gla.ac.uk";
	private String posturl = "";
	private String[] months =  {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	private String[] monthNum = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
	private String upload = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_tags);		
		
		urls[0] = "http://ptolemy.astro.gla.ac.uk/podcasting/track/a2sr";
		name[0] = "Astronomy 2, Special Relativity";
		
		available = new HashMap<String, ArrayList<String>>();
		tagList = new ArrayList<String>();
		lectures = new ArrayList<String>();
		
		//call async task to load all the available lectures for all of the courses
		for(int i = 0; i<1; i++){
			//
			String[] url = new String[]{urls[i]};
			new availableLectures().execute(url);
			//available.put(name[i], lectures);
		}
		
		//parseLectures();
		//createList();  	
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// this creates the menu, depending on the items in the home menu
		getMenuInflater().inflate(R.menu.activity_home, menu);
		return true;
	}

	
	/** BEGIN SECTION - HANDLE MENU BUTTON CLICKS **/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
	    case R.id.button_home:
	        home();
	        return true;
	    case R.id.my_tags:
	    	myTags();
	    	return true;
	    default:
	        return super.onOptionsItemSelected(item);
		}
	}
	   
	public void home(){
			Intent intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
		}

		
    public void myTags() {
        	
        	Intent intent = new Intent(this, MyTagsActivity.class);
            startActivity(intent);   
        }
		
	/** END SECTION **/
        
    //called when the user decides to upload the tags
        
    public void createList(){
    	SharedPreferences sp = this.getSharedPreferences("unsavedTags",0);
		boolean exists = sp.contains("unsavedTags");
     
		  
				if(exists){
					String s = sp.getString("unsavedTags", "");
					
					if(s ==""){
						Toast.makeText(getApplicationContext(),"There are no tags to upload at the moment.", Toast.LENGTH_LONG).show();
					}
					else{
					String[] str = s.split("END"); 

					
					for(int i=0; i < str.length; i ++){
							String[] t = str[i].split(";");
							
							tagList.add(t[3] + "- " + t[1] + "- " + t[4]);
							
					}
					}
						
				}
				
		
	   //create and populate the list view
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
			    android.R.layout.simple_list_item_multiple_choice, tagList);
	  
        listview = (ListView) findViewById(R.id.listview_mytags);
			  // Assign adapter to ListView
        listview.setAdapter(dataAdapter);
    }
    
    public String parseLectures(String lectureDate){
    	
    	//Toast.makeText(getApplicationContext(), "parsing..." + lectures.size(), Toast.LENGTH_LONG).show();
    	
    	//for each of the available lectures 
    	//parse the html string that is returned and change the array list that is stored
    	
    	//in the new array list store url, date
    	
    	boolean found = false;
    	String lecture = "NONE";
    	int i=1;
    	
    	while(i<lectures.size() && !found){ //loop until the end of all lecture or it finds the correct lecture
    		
    		String s = lectures.get(i);
    		
    		String[] temp = s.split("start time:");
    		int end = temp[1].indexOf(";");
    		String date = temp[1].substring(0, end);
    		
    		found = compDates(date, lectureDate);
    		
    		//if they are the same then get the url and return this
    		if (found){   			
    			temp = temp[0].split("href=");
    			
    			end = temp[1].indexOf(">");
    			
    			lecture = temp[1].substring(1, end-1);
    			//Toast.makeText(getApplicationContext(),"Found " + lecture, Toast.LENGTH_LONG).show();
    		}    		
    		
    		i++;
    	}
    		
    	return lecture;
    	
    	
    }
        
    public void uploadTags (View view) {
    	
    //try to upload all of the tags in the list veiw
    	    	
   // int x = listview.getCheckedItemCount();
		
		Toast.makeText(getApplicationContext(), "tag list size " + tagList.size() , Toast.LENGTH_SHORT).show();
		
		//SparseBooleanArray s = listview.getCheckedItemPositions();
		
	
		//for each of the tags to be added find if there is an available lecture for it
    	for (int i =0; i < tagList.size(); i++){
						
			//uploadTags.add(tagList.get(temp));
			
			String[] l = tagList.get(i).split("- ");
			String name = l[1];
			String date = l[2];
				
			//we want it in the format day, date, month
			String[] t = date.split(" ");
			
			date = t[0] + " " + t[2] + " " + t[1];
			
			//see if there is a matching lecture		
			String found = parseLectures(date);
			
			if(found != "NONE"){
				//then there is a matching lecture
				//create the string to send to the login page
				//should be the URL + the tag string  with tag string in the format :
	 		  	// "{\"events\": [{\"start\": \"2012-09-19T11:15:00\", \"tags\": [\"one\"]}]}"
				
				String d = formatDate(l[2]);
				String u = "URL=" + baseURL+found+"~"+"{\"events\": [{\"start\": \""+d+"\", \"tags\": [\""+l[0]+"\"]}]};";
				
				String[] up = {baseURL+found, "{\"events\": [{\"start\": \""+d+"\", \"tags\": [\""+l[0]+"\"]}]}"};
				
				uploadTags.add(up);
				
				//Toast.makeText(getApplicationContext(), u , Toast.LENGTH_LONG).show();
				upload=upload+u;
			}
			
		}
    	
    	if(upload == ""){
    		//There is nothing to upload
    		Toast.makeText(getApplicationContext(), "There are no tags to upload at the moment. Please try again later." , Toast.LENGTH_LONG).show();
    	}
    	else{
		//pass upload tags as a message to be retrieved by the next class
    	Intent intent = new Intent(this, TagTalkLoginActivity.class);
    	intent.putExtra("tags", upload);
        startActivity(intent);  
    	}
    }
    
    public String formatDate(String d){
    	// requied format yyyy-mm-dd'T'hh:mm:ss
    	
    	String[] temp = d.split(" ");
    	
    	String m = getMonthNum(temp[1]);
    	String date = temp[5] + "-" + m + "-" + temp[2] + "T" + temp[3];
  
    	return date;
    }
    
    private String getMonthNum(String m){
    	
    	boolean found = false;
    	int x = 0;
    	int r = 0;
    	
    	while(found == false) {
    		if (m.equals(months[x])){
    			//we have found the correct month
    			found = true;
    			r = x;
    		}
    		x++;
    	}
    	
    	return monthNum[r];
    }
    
    private boolean compDates(String d1, String d2){
    	//d1 is the date of the current lecture 
    	//d2 is the date of the tag
    	
    	int end = d1.indexOf(",");
    	d1 = d1.substring(1, end-5); //removes the date and the white space
    	    	
    	boolean result = d1.equalsIgnoreCase(d2);
    	//String s = Boolean.toString(result);
    	//Toast.makeText(getApplicationContext(), "Found??"+d1 +"XXX" + d2 + s, Toast.LENGTH_LONG).show();
    
    	return result;
    }
   
    private class availableLectures extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
        	 try {
        		    HttpClient client = new DefaultHttpClient();  
        		    //String getURL = "http://ptolemy.astro.gla.ac.uk/podcasting/track";
        		    HttpGet get = new HttpGet(params[0]);
        		   
        		    HttpResponse responseGet = client.execute(get);  
        		    HttpEntity resEntityGet = responseGet.getEntity();  
        		    if (resEntityGet != null) {  
        		        // do something with the response
        		        String response = EntityUtils.toString(resEntityGet);
        		        return response;
        		    }
        	 }
        	catch(Exception e){
              
        	}
        	 return "message";
        }      
        
        @Override
        protected void onPostExecute(String message){
        	
        	String[] all = message.split("<dt>");     	
        	
	        
	        for (int i=0; i<all.length; i++){
	        	lectures.add(all[i]);           		        	
        	}
	        
	       // Toast.makeText(getApplicationContext(), "Compiled list "+ lectures.size(), Toast.LENGTH_LONG).show();
	        createList();       
	    
        	 
        }
    }

}

    
 
    




