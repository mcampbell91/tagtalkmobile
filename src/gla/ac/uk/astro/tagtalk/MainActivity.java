package gla.ac.uk.astro.tagtalk;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	public final static String filename = "users";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences users = this.getSharedPreferences(filename,0);
        String cur = users.getString("currentUser", "NONE");
        
       
        
       // if(cur.equalsIgnoreCase("NONE")){
        	//then there is no current user
            //So change to the login activity class
        //	Intent intent = new Intent(this, LoginActivity.class);
        	//startActivity(intent);
        //}
        //else{
        	//there is a user logged in already so load the home page
        	      	 
        	Intent intent = new Intent(this, HomeActivity.class);
        	startActivity(intent);
        //}
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	//this page doesn't need a menu
    	//so returns true to show completion
        return true;
    }
}
