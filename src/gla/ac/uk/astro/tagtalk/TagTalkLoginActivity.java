package gla.ac.uk.astro.tagtalk;




import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EncodingUtils;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

public class TagTalkLoginActivity extends Activity {
	
/** Called when the activity is first created. */
	
WebView wv; 
String[][] uploadtags;

@SuppressLint("NewApi")
@Override
public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_tag_talk_login);
	
	/** Here get the message and parse it using an async task while the user logs in **/
	
	//StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//	StrictMode.setThreadPolicy(policy); 
	
	Bundle extra = getIntent().getExtras();
	
	String tags = extra.getString("tags");
	
	wv = (WebView)findViewById(R.id.my_webview);
	wv.setWebViewClient(new WebViewClient() {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	});
		
	wv.loadUrl("http://ptolemy.astro.gla.ac.uk/podcasting/user"); 
	getTags(tags);

 }



@Override
public boolean onCreateOptionsMenu(Menu menu) {
	// this creates the menu, depending on the items in the home menu
	getMenuInflater().inflate(R.menu.activity_home, menu);
	return true;
}


/** BEGIN SECTION - HANDLE MENU BUTTON CLICKS **/
@Override
public boolean onOptionsItemSelected(MenuItem item) {
	switch (item.getItemId()) {
    case R.id.button_home:
        home();
        return true;
    case R.id.my_tags:
    	myTags();
    	return true;
    default:
        return super.onOptionsItemSelected(item);
	}
}
   
public void home(){
		Intent intent = new Intent(this, HomeActivity.class);
		startActivity(intent);
	}

	
public void myTags() {
    	
    	Intent intent = new Intent(this, MyTagsActivity.class);
        startActivity(intent);   
    }
	
/** END SECTION **/


public void getTags(String tags){
	
	String[] t = tags.split(";");
	
	uploadtags = new String [t.length] [2];	
	
	for(int i =0; i<t.length; i++){
		//Toast.makeText(getApplicationContext(),, Toast.LENGTH_LONG).show();
		String[] temp = t[i].split("~");
		uploadtags[i][0] = temp[0].substring(4);
		uploadtags[i][1] = temp[1];
		//Toast.makeText(getApplicationContext(), "tags=" + temp[1] +"TEST", Toast.LENGTH_LONG).show();
	}
	
	}

public String getUrl(){
	String html = null;
	
	try {
	    HttpClient client = new DefaultHttpClient();  
	    String getURL = "http://www.astro.gla.ac.uk/podcasting/user";
	    HttpGet get = new HttpGet(getURL);
	    HttpResponse responseGet = client.execute(get);  
	    HttpEntity resEntityGet = responseGet.getEntity();  
	    if (resEntityGet != null) {  
	        // do something with the response
	        String response = EntityUtils.toString(resEntityGet);
	        Log.i("GET RESPONSE", response);
	        html = response;
	    }
	} 
	catch (Exception e){
		//Toast.makeText(getApplicationContext(), "Excetion " + e.getMessage(), Toast.LENGTH_LONG).show();
	}
	return html;
}

 	public void continueToUpload(View view) {
		
		//Intent intent = new Intent(this, UploadTagsActivity.class);
		//startActivity(intent);  
		
		postData();
	}
 	
 	public void getHeader(String url){
 		
 		HttpClient client = new DefaultHttpClient();  
 		
 		CookieManager c = CookieManager.getInstance();
 		String a = c.getCookie(url);
 		//Toast.makeText(getApplicationContext(), "cookie=  " + a, Toast.LENGTH_LONG).show();
 		
	    try{
	    HttpGet get = new HttpGet(url);
	    HttpResponse responseGet = client.execute(get);  
	    HttpEntity resEntityGet = responseGet.getEntity();  
	    if (resEntityGet != null) {  
	        // do something with the response
	        String response = EntityUtils.toString(resEntityGet);
	        Log.i("GET RESPONSE", response);
	        //Toast.makeText(getApplicationContext(), "Response =  " + response, Toast.LENGTH_LONG).show();
	        
	        Header[] h = responseGet.getAllHeaders();
	        
	      //  Toast.makeText(getApplicationContext(), "header numbers=  " + h.length, Toast.LENGTH_LONG).show();
	        
	        for(int i = 0; i< h.length; i ++){
	        //	 Toast.makeText(getApplicationContext(), "header number=  " + i + "header = " + h[i] , Toast.LENGTH_LONG).show();
	        }
	        
	    }
	    }
	    catch(Exception e) {
	    	//Toast.makeText(getApplicationContext(), "Exception" + e.getMessage(), Toast.LENGTH_LONG).show();
	    }
	} 
 		
 	
	
	public void postData() {
	    // Create a new HttpClient and Post Header
		
		Toast.makeText(getApplicationContext(), "Uploading your tags, this may take a few moments.", Toast.LENGTH_LONG).show();
				
		//get the authorisation cookie
		String url = wv.getUrl();		
		CookieManager c = CookieManager.getInstance();
 		String auth = c.getCookie(url);
 		
 		//for each tag to be uploaded
 		
 		for(int i =0; i<uploadtags.length; i ++){
 		//create the array to send to the server request
 			
 		String[] params = new String[]{uploadtags[i][0], uploadtags[i][1], auth};
 		new postTags().execute(params);
 		}
	    
 		//set the shared prefs to be null
 		
	} 
	
    private class postTags extends AsyncTask<String, Void, String> {
    	//passes in the url and the string to create the JSON with in an arry
    	//params[1] is the JSON string
    	//params[0] is the posting url
    	//params[2] is the cookie

        @Override
        protected String doInBackground(String... params) {
        	
        	//Toast.makeText(getApplicationContext(), "JSON = "+ params[1], Toast.LENGTH_SHORT).show();
        	//Toast.makeText(getApplicationContext(), "url= " + params[0], Toast.LENGTH_SHORT).show();
        	//Toast.makeText(getApplicationContext(), "cookie= " + params[2], Toast.LENGTH_SHORT).show();
        	
     		URL urlReal;
    		try {
    			urlReal = new URL(params[0]);
    			
    			HttpURLConnection connection;
    			try {
    				connection = (HttpURLConnection) urlReal.openConnection();
    				
    				connection.setDoOutput(true);

                    connection.setConnectTimeout(10000);  
                    connection.setReadTimeout(10000);  
    		 		connection.setRequestMethod("POST");
    		 		connection.setRequestProperty("Cookie", params[2]);
    		 		connection.setRequestProperty("Accept", "text/html");
    			    connection.setRequestProperty("Content-type", "application/json");
    		 		connection.connect();
    		 		
    		 		  DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());

    		 		  
    		 		    //JSONObject j = new JSONObject("{\"event\": 1}");
    		 		  	//String u = "(date \"2012-09-19T11:15:11\" (tags \"hello\"))";
    		 		  
    		 		  	//create a JSON object from a string should be in the format:
    		 		  	// "{\"events\": [{\"start\": \"2012-09-19T11:15:00\", \"tags\": [\"one\"]}]}"
    					
    					//JSONObject j = new JSONObject("{\"events\": [{\"start\": \"2012-09-19T11:15:00\", \"tags\": [\"test\"]}]}");
    					JSONObject j = new JSONObject(params[1]);
    					//Toast.makeText(getApplicationContext(),j.toString(), Toast.LENGTH_LONG).show();
    					
    					//String tag = "(event (date \"2010-06-21T11:32:13\") (tags \"TEST\"))";
    					
    		 		    wr.writeBytes(j.toString());

    		 		    wr.flush();
    		 		    wr.close();	 		   
    		 		   
    		 		 //build the string to store the response text from the server
    		 		   String response= "";

    		 		   //start listening to the stream
    		 		  InputStream inStream = connection.getInputStream();
    		 		  byte[] buffer = new byte[1024];
    		 		  
    		 		  inStream.read(buffer);
    		 		  
    		 		  response = new String(buffer, "US-ASCII");
    		 		  return connection.getResponseMessage() + " " + response;
    		 		    			
    		 		    		 		
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    				return "exception";
    			} catch (JSONException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    				return "exception " + params[1];
    			}
    			
    		} catch (Exception e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    			return "exception";
    		}
        }      
        
        protected void onPostExecute(String message){
        	if(message == "exception"){
        		Toast.makeText(getApplicationContext(),"Some tags could not be uploaded at this time. Please ensure you are logged in correctly or try again later.", Toast.LENGTH_LONG).show();
        	}
        	else{
            	Toast.makeText(getApplicationContext()," Tag uploaded successfully.", Toast.LENGTH_SHORT).show();
            }
      
        		
        	}
        }
    }
	
