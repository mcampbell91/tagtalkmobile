package gla.ac.uk.astro.tagtalk;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity {

	public String matric;
	public String name;
		
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	//this page doesn't need a menu
    	//so returns true to show completion
        return true;
    }
    /** Called when the user clicks the login button */ 
    public void login(View view) {
    	
    	// get the users details as they were entered
    	EditText temptext = (EditText) findViewById(R.id.matric_number);
    	matric = temptext.getText().toString();
    	
    	temptext = (EditText) findViewById(R.id.name);
    	name = temptext.getText().toString();
    	
    	SharedPreferences users = this.getSharedPreferences("users",0);
    	users.edit().putString("currentUser", matric).commit();
    	
    	// add this students details to the shared preferences
    	boolean worked = users.edit().putString(matric, name).commit();
	   
    	Intent intent = new Intent(this, HomeActivity.class);
    	startActivity(intent); 
    } 
    
}
