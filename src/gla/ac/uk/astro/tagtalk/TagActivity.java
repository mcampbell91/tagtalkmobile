package gla.ac.uk.astro.tagtalk;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;

import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class TagActivity extends Activity {

	/** This class maintains a new list of tags for this current session
	 * when a user navigates away from this page the list of current tags
	 * is added to the file for that user.
	 */
	
	
	/* Enumerations of the menu items */
	private enum menu {HOME,MY_TAGS};
	
	public static String MESSAGE = "";
	private static String filename;
	private static String filenameSP = "users";
	private String tags = "";
	private String message = "Some of your tags have not been saved yet, if you continue these tags will be lost. " +
			"Would you like to save these before continuing?";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/* get the current user and time and create a tag file for them */
		SharedPreferences users = this.getSharedPreferences(filenameSP,0);
        String cur = users.getString("currentUser", "NONE");
        filename = cur + "tags.txt";
		setContentView(R.layout.activity_tag);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_tag, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
	    case R.id.button_home:
	        unsavedTags(TagActivity.menu.HOME);
	        return true;
	    case R.id.my_tags:
	    	unsavedTags(TagActivity.menu.MY_TAGS);
	    	return true;
	    default:
	        return super.onOptionsItemSelected(item);
		}
	}
	
	/** BEGIN SECTION - HANDLE MENU BUTTON CLICKS **/
	   
	public void home(){
			Intent intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
		}

		
    public void myTags() {
        	
        	Intent intent = new Intent(this, MyTagsActivity.class);
            startActivity(intent);   
        }
		
	/** END SECTION **/

		
	/** BEGIN SECTION - BUTTONS */
	
	/* Called when the user clicks the lost button */
    public void sendLostMessage(View view) {
    	
        /* Add this to the current list of added tags */
    	tags = tags + generateMessage("Lost");
    	Toast.makeText(getApplicationContext(), "Lost Tag Added Successfully.", Toast.LENGTH_SHORT).show();
    
    }
    
    /** Called when the user clicks the interesting button */
    public void sendInterestingMessage(View view) {
    	
    	/* add this to the current list of tags being maintained */
    	tags = tags + generateMessage("Interesting");
    	Toast.makeText(getApplicationContext(), "Interesting Tag Added Successfully.", Toast.LENGTH_SHORT).show();
    }
    
    public void saveTags (View View){
    	/* here we simply save the tags to the file
    	 * on the close of the application they send it to the server on close
    	 * 
    	 * 
    	 * NB. only one file per user. If they navigate to another 
    	 * lecture in the same session then all previous tags are 
    	 * lost. This can be fixed using shared preferences or having
    	 * the server able to handle multiple filenames
    	 */
    	
    	addTagsToFile();
    	
    	Intent intent = new Intent(this, HomeActivity.class);
    	startActivity(intent);
    }
    /** END SECTION **/
    
    /** BEGIN SECTION HELPER METHODS **/
    public String generateMessage(String type){
    	String message = "";
    	
    	//get the time the tag was added at
    	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	
 	    //Date date = new Date();
 	    
    	/** This code adds the tags to this time for testing purposes **/
    	
    	String [] times = {"2012/09/19 11:14:11", "2012/09/19 11:17:11","2012/09/19 11:22:11" ,"2012/09/19 11:26:11" ,"2012/09/19 11:38:11"};
    	Date date;
    	Random rand = new Random();
    	int tagtime = rand.nextInt(5);
    	
    	Toast.makeText(getApplicationContext(), "tag time = " + tagtime, Toast.LENGTH_LONG).show();
    	
		try {
			date = dateFormat.parse(times[tagtime]);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			date = new Date();
		}
    	
    	
 	    //get the lecture tagging
 	    Intent intent = getIntent();
 	    String lecture = intent.getStringExtra(HomeActivity.MESSAGE); 
 	    
 	    //get the user
 	   SharedPreferences users = this.getSharedPreferences(filenameSP, 0);
 	   String cur = users.getString("currentUser", "NONE");
 	    
 	    //generate messages
 	    //STILL TO ADD - start time, tagger name,  
 	    message = cur +";" + lecture + ";START TIME;" + type + ";" + date.toString() +";" + 
 	    date.getTime()+" END";

 	    return message;
    }
    
    public void addTagsToFile(){
    	//attempt to write to file within try and catch blocks
    	//user can then be altered of the success of their save of tags
 
        try {            
            
        	/* Saving to a file code 
        	 * THIS IS NO LONGER USED 
        	 * //open the file        
        	 
            FileOutputStream file = openFileOutput(filename, 0);
            OutputStreamWriter writer = new OutputStreamWriter(file); 

            // Write the string to the file and close it
            writer.write(tags);
            writer.flush();
            writer.close(); */
            
            //save the tags to the shared preferences 
            //this is easier to access than the file
            
           SharedPreferences t = this.getSharedPreferences("unsavedTags", 0);      	   
      	   t.edit().putString("unsavedTags", tags).commit();
            
           //ADD CODE TO DISLAY SUCCESS HERE AS ALERT
            Toast.makeText(getApplicationContext(), "Tags saved successfully.", Toast.LENGTH_SHORT).show();

        }
        catch(Exception e){
        	//ADD CODE TO DISPLAY FAILURE HERE AS ALERT
        	Toast.makeText(getApplicationContext(), "Tags Save Failed. Please try again. " + e.getClass(), Toast.LENGTH_LONG).show();
        }
        
    }
    /** END SECTION **/

    /** BEGIN SECTION - HANDLE MENU BUTTON CLICKS **/
    
    private void unsavedTags(final TagActivity.menu load){
    	/*This determines if the user wishes to save the tags or delete them
    	 * It creates and alert box and then waits for the user to decide
    	 * once a button has been clicked it reloads the correct intent*/
                     
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
         
        // set the message an heading to display
        alertbox.setTitle("Unsaved Tags!").setMessage(message);
         
        //add the buttons and their actions on click
        alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface dia, int i) {
                // we want to save the tags
            	//so add to file and then load the new intent
            	addTagsToFile();
            	//Toast.makeText(getApplicationContext(), "Yes button clicked " + load.toString(), Toast.LENGTH_SHORT).show();
            	loadIntent(load);
            }
        });
        
        alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dia, int i) {
                // don't want to save the tags
            	//so just load the intent this time
                //Toast.makeText(getApplicationContext(), "No button clicked " + load.toString(), Toast.LENGTH_SHORT).show();
                loadIntent(load);
            }
        });
         
        // Now display the alert to the user
        alertbox.show();
    }	
    
    private void loadIntent(TagActivity.menu load){
		switch (load) {
	    case HOME:
            //Toast.makeText(getApplicationContext(), "Load " + load.toString(), Toast.LENGTH_SHORT).show();
	    	home();
            break;
	    case MY_TAGS:
	    	//Toast.makeText(getApplicationContext(), "Load " + load.toString(), Toast.LENGTH_SHORT).show();
	    	myTags();
	    	break;
	    default:
	    	home();
		}
    }
   
     
   public void logout(){
    	      	
   	//logout the current user
       SharedPreferences users = this.getSharedPreferences("users",0);
       users.edit().putString("curentUser", "NONE").commit();
       
       //reload the login page
       Intent intent = new Intent(this, LoginActivity.class);
       startActivity(intent);            
   }
   
	public void courses(){
		//unsavedTags(COURSES);
   	TextView textView = new TextView(this);
	    textView.setTextSize(40);
	    textView.setText("My Tags");
	    setContentView(textView);
   }
	
   public void myDetails() {
   	//unsavedTags(DETAILS);
   	TextView textView = new TextView(this);
	    textView.setTextSize(40);
	    textView.setText("My Details");
	    setContentView(textView);
   }
	
/** END SECTION **/

}
