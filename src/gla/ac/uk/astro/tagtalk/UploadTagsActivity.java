package gla.ac.uk.astro.tagtalk;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.StrictMode;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class UploadTagsActivity extends Activity implements OnItemSelectedListener {
	
	
	/** This is the screen that lets the user select the lecture they wish to tag
	 *  Needs to read the time from the time selector
	 */
	public String lecture;
	public String time;
	public Date day = new Date();
	public static String MESSAGE = "";
	private ListView listview;
	private ArrayList<String> tagList;
	private ArrayList<String> uploadTags;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_upload_tags);
		
		//set the properties so that requests are allowed
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
		
		populateList();
		
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// this creates the menu, depending on the items in the home menu
		getMenuInflater().inflate(R.menu.activity_home, menu);
		return true;
	}

	
	/** BEGIN SECTION - HANDLE MENU BUTTON CLICKS **/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
	    case R.id.button_home:
	        home();
	        return true;
	    case R.id.my_tags:
	    	myTags();
	    	return true;
	    default:
	        return super.onOptionsItemSelected(item);
		}
	}
	   
	public void home(){
			Intent intent = new Intent(this, HomeActivity.class);
			startActivity(intent);
		}

		
    public void myTags() {
        	
        	Intent intent = new Intent(this, MyTagsActivity.class);
            startActivity(intent);   
        }
		
	/** END SECTION **/
	
	
	private void populateList(){			 
			  //Array list of countries
			  tagList = new ArrayList<String>();
			  
			  tagList = getTags();
			  			 
			  //create an ArrayAdaptar from the String Array
			  
			  ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
					    android.R.layout.simple_list_item_multiple_choice, tagList);
			  
			  listview = (ListView) findViewById(R.id.listview);
					  // Assign adapter to ListView
			  listview.setAdapter(dataAdapter);  	  
	}
		
	public ArrayList<String> getTags(){
		
		SharedPreferences sp = this.getSharedPreferences("unsavedTags",0);
		boolean exists = sp.contains("unsavedTags");
     
		  
				if(exists){
					String s = sp.getString("unsavedTags", "");
					
					String[] str = s.split("END"); 

					
					for(int i=0; i < str.length; i ++){
							String[] t = str[i].split(";");
							
							tagList.add(t[4] + ", " + t[1]);
					}
						
				}
			return tagList;
	}
	
	public void uploadSelected( View view ) {
		
		int x = listview.getCheckedItemCount();
		
		Toast.makeText(getApplicationContext(), "Checking for tags to upload, this may take a few moments", Toast.LENGTH_SHORT).show();
		
		SparseBooleanArray s = listview.getCheckedItemPositions();
	
		for (int i =0; i < x; i++){
			int temp = s.keyAt(i);
			uploadTags.add(tagList.get(temp));
			
			//Toast.makeText(getApplicationContext(), "items " + tagList.get(temp) , Toast.LENGTH_SHORT).show();
		
		}
		
		postData();
	}	
	
	
	public void postData() {
	    // Create a new HttpClient and Post Header
	    
		JSONObject j;
		
	    try {
			j = new JSONObject("{\"likes\": 1}");
			//Toast.makeText(getApplicationContext(), "JSON created " + j.getString("likes"), Toast.LENGTH_SHORT).show();
			
			//instantiates httpclient to make request
		    
		    HttpClient httpclient = new DefaultHttpClient();
		    HttpPost httppost = new HttpPost("http://www.astro.gla.ac.uk/podcasting/event/t1/moodle");


		    //passes the results to a string builder/entity
		    StringEntity se;
			try {
				se = new StringEntity(j.toString());
				
				 //sets the post request as the resulting string
			    httppost.setEntity(se);
			    //sets a request header so the page receving the request
			    //will know what to do with it
			    
			  //  String auth = android.util.Base64.encodeToString(
			    //		("0901468c" + ":" + "sb51mdv").getBytes("UTF-8"), 
			    	//	android.util.Base64.NO_WRAP
			    		// );
			    //httppost.addHeader("Authorization", "Basic "+ auth);	
			    
			    httppost.setHeader("Accept", "text/html");
			    httppost.setHeader("Content-type", "application/json");

			    //Handles what is returned from the page 
			    ResponseHandler responseHandler = new BasicResponseHandler();
			    try {
			    	HttpResponse r = httpclient.execute(httppost);
			    			    	
			    	Toast.makeText(getApplicationContext(), "Response " + r.getStatusLine().getStatusCode(), Toast.LENGTH_SHORT).show();
			    	Toast.makeText(getApplicationContext(), "Response " + r.getStatusLine().getReasonPhrase(), Toast.LENGTH_SHORT).show();
			    	
			    	//this allows you to see the servers response :)
			    	String responseBody = EntityUtils.toString(r.getEntity());
			    	
			    	Toast.makeText(getApplicationContext(), "Response " + responseBody, Toast.LENGTH_SHORT).show();
			    	
			    	TextView textView = (TextView)findViewById(R.id.response);
		    	    textView.setText(responseBody);
		    	    
			    } catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					Toast.makeText(getApplicationContext(), "Client exception " + e.toString() + "\n "+ httpclient.getParams(), Toast.LENGTH_LONG).show();
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					Toast.makeText(getApplicationContext(), "IO exception " + e.getMessage(), Toast.LENGTH_SHORT).show();
					e.printStackTrace();
				}
				
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				Toast.makeText(getApplicationContext(), "Encoding exception", Toast.LENGTH_SHORT).show();
				e1.printStackTrace();
			}

		   
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			Toast.makeText(getApplicationContext(), "JSON exception", Toast.LENGTH_SHORT).show();
			e1.printStackTrace();
		}
	    
	  
	    
	    /**
	    
	    try {
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("{likes : 1}", ""));
	        //nameValuePairs.add(new BasicNameValuePair("stringdata", "AndDev is Cool!"));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        
	        StatusLine s = response.getStatusLine();
	        Locale l = response.getLocale();
	        HttpEntity m = response.getEntity();
	        InputStream i = m.getContent();
	        byte[] buffer = null;
	        i.read(buffer, 0, 500);
	        
	        Toast.makeText(getApplicationContext(), "response = " + s + " locale = " + l + " message = " + buffer, Toast.LENGTH_SHORT).show();
	        
	        
	    } catch (ClientProtocolException e) {
	        // TODO Auto-generated catch block
	    	Toast.makeText(getApplicationContext(), "client exception " , Toast.LENGTH_SHORT).show();
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	    	Toast.makeText(getApplicationContext(), "IO Exception " , Toast.LENGTH_SHORT).show();
	    }**/
	} 
	

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

}
